package com.fpg.geolocation.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.fpg.geolocation.models.FPServiceConfiguration;
import com.fpg.geolocation.models.FPServiceState;
import com.fpg.geolocation.utility.FPServiceTracker;

/**
 * Created by Axel Trajano on 2/26/20.
 */
public class FPBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        FPServiceTracker.setServiceState(context, FPServiceState.BOOTED);
        FPLocationManager.getInstance(context, null,
                FPServiceConfiguration.load(context), null);
    }
}
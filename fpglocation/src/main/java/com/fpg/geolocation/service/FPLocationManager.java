package com.fpg.geolocation.service;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fpg.geolocation.models.FPLocationError;
import com.fpg.geolocation.models.FPServiceConfiguration;
import com.fpg.geolocation.models.FPServiceState;
import com.fpg.geolocation.network.HTTPListener;
import com.fpg.geolocation.protocol.FPLocationManagerDelegate;
import com.fpg.geolocation.utility.FPServiceStatus;
import com.fpg.geolocation.utility.FPServiceTracker;

import java.io.Serializable;


/**
 * The primary interface for receiving location events.
 *
 * Created by Axel Trajano on 2/29/20.
 */
public class FPLocationManager implements HTTPListener {
    private final String TAG = this.getClass().getSimpleName();

    private Context context;
    private FPLocationManagerDelegate delegate;
    private Location lastLocation;
    private FPServiceConfiguration configuration;

    // A reference to the service used to get location updates.
    private FPLocationService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    private static FPLocationManager instance;

    /**
     * Obtaining an insatnce of the FPLocationManager
     *
     * @param context
     * @param activityClass
     * @param configuration
     * @param delegate
     * @return
     */
    public static FPLocationManager getInstance(Context context, Class activityClass,
                                                FPServiceConfiguration configuration,
                                                FPLocationManagerDelegate delegate) {
        if (instance == null) {
            instance = new FPLocationManager(context.getApplicationContext(), configuration, delegate);
        }
        if (activityClass != null) configuration.setActivityClassName(activityClass.getName());
        instance.setConfiguration(configuration);
        instance.delegate = delegate;

        return instance;
    }

    private FPLocationManager(Context context,
                              FPServiceConfiguration configuration,
                             FPLocationManagerDelegate delegate) {
        this.context = context.getApplicationContext();
        this.delegate = delegate;

        if (configuration != null) {
            this.configuration = configuration;
            configuration.save(context);
        } else {
            this.configuration = FPServiceConfiguration.load(context);
        }

        // Register the receivers for the location service broadcasts
        LocalBroadcastManager local = LocalBroadcastManager.getInstance(context);

        IntentFilter updater = new IntentFilter(FPLocationService.BROADCAST_KEY_LOCATION);
        local.registerReceiver(locationUpdateReceiver, updater);

        IntentFilter failer = new IntentFilter(FPLocationService.BROADCAST_KEY_ERROR);
        local.registerReceiver(locationFailureReceiver, failer);

        bindService(context);

        Log.i(TAG, "Location Manager Initiated");
    }

    private BroadcastReceiver locationFailureReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Location Failure Receiver");

            String errorKey = FPLocationService.BROADCAST_KEY_ERROR;
            Serializable serial = intent.getSerializableExtra(errorKey);
            if (delegate != null) delegate.onLocationManagerFailure((FPLocationError) serial);
        }
    };

    private BroadcastReceiver locationUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Location Update Receiver");

            Location location = intent.getParcelableExtra(FPLocationService.BROADCAST_KEY_LOCATION);
            if (location == null) {
                if (delegate != null) delegate.onLocationManagerFailure(FPLocationError.UNKNOWN);
                return;
            }

            // Save the location if enough time has passed or it is the first location
            lastLocation = location;
            if (delegate != null) delegate.onLocationManagerUpdate(location);
        }
    };

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof FPLocationService.LocalBinder) {
                FPLocationService.LocalBinder binder = (FPLocationService.LocalBinder) service;
                mService = binder.getService();
                mBound = true;
                if (delegate != null) delegate.onBind();
                if (FPServiceTracker.getServiceState(context).equals(FPServiceState.BOOTED)) {
                    startForegroundService();
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    public FPLocationManagerDelegate getDelegate() {
        return delegate;
    }

    public void setDelegate(FPLocationManagerDelegate delegate) {
        this.delegate = delegate;
    }

    private void setConfiguration(FPServiceConfiguration configuration) {
        assert configuration != null;
        this.configuration = configuration;
        this.configuration.save(context);
    }

    // HTTP Listener

    @Override
    public <T> void onSuccess(int statusCode, T result) {
        // Nothing to do on success
    }

    @Override
    public void onFail(int statusCode, String message) {
        if (delegate != null) delegate.onLocationManagerFailure(FPLocationError.SERVER);
    }

    @Override
    public void onError(String message) {
        if (delegate != null) delegate.onLocationManagerFailure(FPLocationError.SERVER);
    }

    // Service API

    private void bindService(Context context) {
        context.bindService(new Intent(context, FPLocationService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    /**
     * Request location updates from the service
     */
    public void requestLocationUpdates() {
        Log.d(TAG, "Starting location service");
        mService.requestLocationUpdates();
    }

    /**
     * Remove location updates from the service
     */
    public void removeLocationUpdates() {
        mService.removeLocationUpdates();
    }

    public void unbindService(Context context) {
        Log.d(TAG, "Stopping location service");
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            context.unbindService(mServiceConnection);
            mBound = false;
        }
    }

    private void startForegroundService() {
        if (FPServiceStatus.requestingLocationUpdates(context)) requestLocationUpdates();
        FPServiceTracker.setServiceState(context, FPServiceState.STARTED);
    }
}

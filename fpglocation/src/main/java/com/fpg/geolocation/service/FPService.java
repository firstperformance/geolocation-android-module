package com.fpg.geolocation.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.util.Log;

import com.fpg.geolocation.models.FPServiceConfiguration;
import com.fpg.geolocation.network.HTTPListener;
import com.fpg.geolocation.network.HTTPManager;
import com.fpg.geolocation.protocol.FPServiceNotifier;

import org.json.JSONObject;

import java.util.Date;

/**
 * The parent class for library related services.
 *
 * Created by Axel Trajano on 3/2/20.
 */
abstract class FPService extends Service implements FPServiceNotifier {
    static final String EXTRA_STARTED_FROM_NOTIFICATION = FPService.class.getSimpleName();
    protected Location currentLocation;

    @Override
    public Notification.Builder getNotificationBuilder(Context context) {
        String TAG = getClass().getSimpleName();
        FPServiceConfiguration configuration = FPServiceConfiguration.load(context);
        String activityClassName = FPServiceConfiguration.load(context).getActivityClassName();
        Class activityClass = FPLocationService.class;
        try {
            activityClass = Class.forName(activityClassName);
        } catch(Exception e) {
            Log.d(TAG, e.toString());
        }

        Intent activityIntent = new Intent(context, activityClass);
        activityIntent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, activityIntent, 0);

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle(configuration.getNotificationTitle())
                .setSmallIcon(configuration.getNotificationIconId())
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis());

        String notificationMessage = configuration.getNotificationMessage();
        if (!notificationMessage.trim().isEmpty()) builder.setContentText(notificationMessage);

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(TAG); // Channel ID
        }

        return builder;
    }

    /**
     * Returns the {@link Notification} used as part of the foreground service.
     */
    @Override
    public Notification getNotification(Context context) {
        return getNotificationBuilder(context).build();
    }

    /**
     * Sends the location to the server.
     *
     * @param location
     * @param configuration
     * @param httpListener
     */
    protected void sendLocation(Location location, FPServiceConfiguration configuration, HTTPListener httpListener) {
        if (location == null) return;

        JSONObject locationData = null;
        try {
            locationData = new JSONObject();
            locationData.put("latitude", location.getLatitude());
            locationData.put("longitude", location.getLongitude());
            locationData.put("timestamp", new Date(location.getTime()).toString());
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e.toString());
        }

        HTTPManager.request(HTTPManager.RequestMethod.POST, configuration.getBaseURL(),
                configuration.getToken(), configuration.getClientUserId(), locationData, httpListener);
    }
}

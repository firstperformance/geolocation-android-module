package com.fpg.geolocation.service;

import android.Manifest;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fpg.geolocation.models.FPLocationError;
import com.fpg.geolocation.models.FPServiceConfiguration;
import com.fpg.geolocation.utility.FPServiceStatus;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

/**
 * Manages a FusedLocationProviderClient and monitors for location updates.
 * Updates and errors are broadcast locally.
 * 
 * Created by Axel Trajano on 2/29/20.
 */
public class FPLocationService extends FPService {
    private final String TAG = this.getClass().getSimpleName();
    private static final int NOTIFICATION_ID = 20180504;

    // Intent Data Keys
    public static final String BROADCAST_KEY_ERROR = "permissionEvent";
    public static final String BROADCAST_KEY_LOCATION = "location";

    // Required Properties
    private FusedLocationProviderClient fusedLPClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private Handler mServiceHandler;
    private boolean mChangingConfiguration = false;
    private NotificationManager mNotificationManager;
    private final IBinder mBinder = new LocalBinder();
    private FPServiceConfiguration configuration;
    private Thread wakeThread;

    // The wifi and wake lock to be used for doze mode
    private PowerManager.WakeLock wakeLock = null;
    private WifiManager.WifiLock wifiLock = null;

    @Override
    public void onCreate() {
        Log.d(TAG, "Service created");

        // check permissions
        if (!hasPermissions()) {
            // callback an option for requesting a permission
            Log.d(TAG, "Permissions Not Granted!");
            notifyLocationServiceDidFail(FPLocationError.PERMISSIONS);
        }

        configuration = FPServiceConfiguration.load(getApplicationContext());

        fusedLPClient = LocationServices.getFusedLocationProviderClient(this);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult == null || locationResult.getLastLocation() == null) {
                    Log.w(TAG, "Location client received a null update");
                    return;
                }

                Log.d(TAG, "Location acquired");
                if (hasCoveredDistance(locationResult.getLastLocation()))
                    notifyLocationServiceDidUpdateLocation(locationResult.getLastLocation());
            }
        };

        createLocationRequest();
        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = TAG + ".channel";
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(TAG, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setSound(null, null);
            mChannel.enableLights(false);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }

        enableLock();

        inittWakeThread();
        wakeThread.start();

        if (FPServiceStatus.requestingLocationUpdates(getApplicationContext())) {
            requestLocationUpdates();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground();
        }

        return START_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfiguration = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()");
        stopForeground(true);
        mChangingConfiguration = false;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfiguration = false;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Last client unbound from service");

        // Called when the last client unbinds from this
        // service. If this method is called due to a configuration change in the Activity , we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && FPServiceStatus.requestingLocationUpdates(this)) {
            Log.i(TAG, "Starting foreground service");
            startForeground();
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        if (FPServiceStatus.requestingLocationUpdates(this)) {
            Log.d(TAG, "restarting foreground service");
            stopWakeThread();
            startForeground(NOTIFICATION_ID, getNotification(this));
        } else {
            mServiceHandler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.i(TAG, "onTaskRemoved");
        if (FPServiceStatus.requestingLocationUpdates(this)) {
            Log.d(TAG, "restarting foreground service");
            startForeground(NOTIFICATION_ID, getNotification(this));
        }
        super.onTaskRemoved(rootIntent);
    }

    public void startForeground() {
        startForeground(NOTIFICATION_ID, getNotification(this));
    }

    private void inittWakeThread() {
        wakeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (FPServiceStatus.requestingLocationUpdates(getApplicationContext())) {
                    try {
                        releaseLock();
                        acquireLock();
                        Thread.sleep(configuration.getInterval());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void stopWakeThread() {
        if (wakeThread != null) {
            wakeThread.interrupt();
            wakeThread = null;
        }
    }

    private void enableLock() {
        // Use wakelock so our service does not get affected by Doze Mode
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        if (powerManager != null) {
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "LocationService::lock");
        }

        // Use a wifi lock so our service does not get affected by Doze Mode
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "MyWifiLock");
        }
    }

    private void acquireLock() {
        wakeLock.acquire(configuration.getInterval() * 60 * 1000L + 2000L);
        wifiLock.acquire();
    }

    private void releaseLock() {
        // Release wakelock when location update is removed
        if (wakeLock != null) {
            if (wakeLock.isHeld())
                wakeLock.release();
        }

        // Release wifi lock when location update is removed
        if (wifiLock != null) {
            if (wifiLock.isHeld())
                wifiLock.release();
        }
    }

    private void notifyLocationServiceDidFail(FPLocationError error) {
        Intent intent = new Intent(BROADCAST_KEY_ERROR);
        intent.putExtra(BROADCAST_KEY_ERROR, error);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    private void notifyLocationServiceDidUpdateLocation(Location location) {
        currentLocation = location;
        Intent intent = new Intent(BROADCAST_KEY_LOCATION);
        intent.putExtra(BROADCAST_KEY_LOCATION, location);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        sendLocation(location, FPServiceConfiguration.load(this), null);

        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
            mNotificationManager.notify(NOTIFICATION_ID, getNotification(this));
        }
    }

    private boolean hasCoveredDistance(Location location) {
        return currentLocation == null || currentLocation.distanceTo(location) >= configuration.getSmallestDisplacement();
    }

    /**
     * Returns TRUE if location permissions have already been granted.
     */
    public boolean hasPermissions() {
        final int GRANTED = PackageManager.PERMISSION_GRANTED;

        String fineKey = Manifest.permission.ACCESS_FINE_LOCATION;
        String coarseKey = Manifest.permission.ACCESS_COARSE_LOCATION;

        boolean hasFine = ActivityCompat.checkSelfPermission(this, fineKey) == GRANTED;
        boolean hasCoarse = ActivityCompat.checkSelfPermission(this, coarseKey) == GRANTED;

        return hasFine || hasCoarse;
    }

    private void createLocationRequest() {
        Log.d(TAG, "Creating Location Request");
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(configuration.getInterval());
        locationRequest.setFastestInterval(configuration.getInterval());
        locationRequest.setSmallestDisplacement(configuration.getSmallestDisplacement());
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void getLastLocation() {
        if (!hasPermissions()) {
            notifyLocationServiceDidFail(FPLocationError.PERMISSIONS);
            removeLocationUpdates();
            return;
        }

        fusedLPClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            Log.d(TAG, "Last location obtained");
                            currentLocation = location;
                        } else {
                            Log.w(TAG, "Location client received a null update");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "GetLastLocation: Failure: " + e.getLocalizedMessage());
                        notifyLocationServiceDidFail(FPLocationError.UNKNOWN);
                    }
                });
    }

    public class LocalBinder extends Binder {
        public FPLocationService getService() {
            return FPLocationService.this;
        }
    }

    /**
     * Makes a request for location updates. Note that in this we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates");
        if (configuration.isSendUpdatesOnExplicitRequests()) {
            currentLocation = null;
        }
        acquireLock();
        FPServiceStatus.setRequestingLocationUpdates(this, true);
        Intent intent = new Intent(getApplicationContext(), FPLocationService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
            startForeground();
        } else {
            startService(intent);
        }
        try {
            fusedLPClient.requestLocationUpdates(locationRequest,
                    locationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            FPServiceStatus.setRequestingLocationUpdates(this, false);
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Log.i(TAG, "Removing location updates");
        FPServiceStatus.setRequestingLocationUpdates(this, false);
        releaseLock();
        try {
            fusedLPClient.removeLocationUpdates(locationCallback);
            stopSelf();
        } catch (SecurityException unlikely) {
            FPServiceStatus.setRequestingLocationUpdates(this, true);
            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }
}

package com.fpg.geolocation.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.fpg.geolocation.service.FPLocationService;

/**
 * Service Configuration
 *
 * Created by Axel Trajano on 2/26/20.
 */
public class FPServiceConfiguration {
    private static final int MILLIS_PER_MINUTE = 60000;
    private String baseURL;
    private String token;
    private String clientUserId;
    private long fastestInterval;
    private long interval;
    private float smallestDisplacement;
    private String activityClassName;
    private String notificationTitle;
    private String notificationMessage;
    private int notificationIconId;
    private boolean sendUpdatesOnExplicitRequests;

    private static final String kBaseURL = "baseURL";
    private static final String kToken = "token";
    private static final String kClientUserId = "clientUserId";
    private static final String kMinMinutes = "minMinutes";
    private static final String kMaxMinutes = "maxMinutes";
    private static final String kSmallestDisplaccement = "smallestDisplacement";
    private static final String kActivityClassName = "activityClassName";
    private static final String kNotifiicationTitle = "notificationTitle";
    private static final String kNotificationMessage = "notificationMessage";
    private static final String kNotificationIcon = "notificationIcon";
    private static final String kSendUpdatesOnExplicitRequests = "explicitRequests";

    private FPServiceConfiguration() {}

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientUserId() {
        return clientUserId;
    }

    public void setClientUserId(String clientUserId) {
        this.clientUserId = clientUserId;
    }

    private long getFastestInterval() {
        return fastestInterval;
    }

    private void setFastestInterval(long fastestInterval) {
        this.fastestInterval = fastestInterval;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public float getSmallestDisplacement() {
        return smallestDisplacement;
    }

    public void setSmallestDisplacement(float smallestDisplacement) {
        this.smallestDisplacement = smallestDisplacement;
    }

    public String getActivityClassName() {
        return activityClassName;
    }

    public void setActivityClassName(String activityClassName) {
        this.activityClassName = activityClassName;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public int getNotificationIconId() {
        return notificationIconId;
    }

    public void setNotificationIconId(int notificationIconId) {
        this.notificationIconId = notificationIconId;
    }

    public boolean isSendUpdatesOnExplicitRequests() {
        return sendUpdatesOnExplicitRequests;
    }

    public void setSendUpdatesOnExplicitRequests(boolean sendUpdatesOnExplicitRequests) {
        this.sendUpdatesOnExplicitRequests = sendUpdatesOnExplicitRequests;
    }

    public void save(Context context) {
        SharedPreferences.Editor pref = context.getSharedPreferences(FPServiceConfiguration.class.getSimpleName(), Context.MODE_PRIVATE).edit();
        pref.putString(kBaseURL, baseURL);
        pref.putString(kToken, token);
        pref.putString(kClientUserId, clientUserId);
        pref.putLong(kMinMinutes, fastestInterval);
        pref.putLong(kMaxMinutes, interval);
        pref.putFloat(kSmallestDisplaccement, smallestDisplacement);
        pref.putString(kActivityClassName, activityClassName);
        pref.putString(kNotifiicationTitle, notificationTitle);
        pref.putString(kNotificationMessage, notificationMessage);
        pref.putInt(kNotificationIcon, notificationIconId);
        pref.putBoolean(kSendUpdatesOnExplicitRequests, sendUpdatesOnExplicitRequests);
        pref.apply();
    }

    public static FPServiceConfiguration load(Context context) {
        SharedPreferences pref = context.getSharedPreferences(FPServiceConfiguration.class.getSimpleName(), Context.MODE_PRIVATE);

        FPServiceConfiguration configuration = new FPServiceConfiguration();
        configuration.setBaseURL(pref.getString(kBaseURL, ""));
        configuration.setToken(pref.getString(kToken, ""));
        configuration.setClientUserId(pref.getString(kClientUserId, ""));
        configuration.setFastestInterval(pref.getLong(kMinMinutes, MILLIS_PER_MINUTE));
        configuration.setInterval(pref.getLong(kMaxMinutes, MILLIS_PER_MINUTE * 2));
        configuration.setSmallestDisplacement(pref.getFloat(kSmallestDisplaccement, 0));
        configuration.setActivityClassName(pref.getString(kActivityClassName, ""));
        configuration.setNotificationTitle(pref.getString(kNotifiicationTitle, FPLocationService.class.getSimpleName()));
        configuration.setNotificationMessage(pref.getString(kNotificationMessage, ""));
        configuration.setNotificationIconId(pref.getInt(kNotificationIcon, android.R.drawable.ic_dialog_map));
        configuration.setSendUpdatesOnExplicitRequests(pref.getBoolean(kSendUpdatesOnExplicitRequests, false));
        return configuration;
    }

}

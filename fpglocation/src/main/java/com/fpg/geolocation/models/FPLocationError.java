package com.fpg.geolocation.models;

public enum FPLocationError {
    UNKNOWN(0, "Location Services or FusedLocationProviderClient encountered an error"),
    PERMISSIONS(1, "Location permissions not missing or denied"),
    SERVER(2, "The server encountered an error");

    private final int code;
    private final String description;

    FPLocationError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return description;
    }
}
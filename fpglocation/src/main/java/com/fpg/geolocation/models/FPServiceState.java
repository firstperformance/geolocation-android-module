package com.fpg.geolocation.models;

/**
 * Service State
 *
 * Created by Axel Trajano on 2/26/20.
 */
public enum FPServiceState {
    BOOTED,
    STARTED,
    STOPPED;
}

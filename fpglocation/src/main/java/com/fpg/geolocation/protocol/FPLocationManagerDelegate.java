package com.fpg.geolocation.protocol;

import android.location.Location;

import com.fpg.geolocation.models.FPLocationError;

/**
 * Interface to be implemented by a location manager delegate to listen for updates.
 *
 * Created by Axel Trajano on 3/2/20.
 */
public interface FPLocationManagerDelegate {
    void onLocationManagerUpdate(Location location);
    void onLocationManagerFailure(FPLocationError error);
    void onBind();
}

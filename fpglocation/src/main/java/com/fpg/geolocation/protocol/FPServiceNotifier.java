package com.fpg.geolocation.protocol;

import android.app.Notification;
import android.content.Context;

/**
 * Interface for implementing Notifications from the service.
 *
 * Created by Axel Trajano on 3/2/20.
 */
public interface FPServiceNotifier {
    Notification.Builder getNotificationBuilder(Context context);
    Notification getNotification(Context context);
}

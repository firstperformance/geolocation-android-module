package com.fpg.geolocation.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.fpg.geolocation.models.FPServiceState;

/**
 * Utility for tracking the service state
 *
 * Created by Axel Trajano on 2/26/20.
 */
public abstract class FPServiceTracker {
    private static final String NAME = FPServiceTracker.class.getSimpleName();
    private static final String SERVICE_KEY = "SERVICE_KEY";

    public static void setServiceState(Context context, FPServiceState state) {
        SharedPreferences.Editor pref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE).edit();
        pref.putString(SERVICE_KEY, state.name());
        pref.apply();
    }

    public static FPServiceState getServiceState(Context context) {
        SharedPreferences pref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return FPServiceState.valueOf(pref.getString(SERVICE_KEY, FPServiceState.STOPPED.name()));
    }
}
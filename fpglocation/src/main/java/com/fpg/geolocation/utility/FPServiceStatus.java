package com.fpg.geolocation.utility;

import android.content.Context;

/**
 * Utility for determining the location service request status.
 *
 * Created by Axel Trajano on 2/29/20.
 */
public abstract class FPServiceStatus {
    private static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";

    public static boolean requestingLocationUpdates(Context context) {
        return context.getSharedPreferences(FPServiceStatus.class.getSimpleName(), Context.MODE_PRIVATE)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        context.getSharedPreferences(FPServiceStatus.class.getSimpleName(), Context.MODE_PRIVATE)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }
}

package com.fpg.geolocation.network;

/**
 * Interface for HTTP callbacks
 * Created by Axel S. Trajano on 14/03/2018.
 */
public interface HTTPListener {
    <T> void onSuccess(int statusCode, T result);
    void onFail(int statusCode, String message);
    void onError(String message);
}

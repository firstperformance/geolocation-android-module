package com.fpg.geolocation.network;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

/**
 * Manages HTTP requests.
 *
 * Created by Bryn Trussell on 17 December 2019.
 */
public abstract class HTTPManager {
    private static final String TAG = HTTPManager.class.getSimpleName();

    private static final String LOCATION_PATH = "%1$s/geolocation/%2$s";

    public enum RequestMethod {
        POST,
        PUT,
        GET,
        DELETE
    }

    public static void request(HTTPManager.RequestMethod method,
                        String baseURL,
                        String token,
                        String clientUserId,
                        JSONObject requestBody,
                        HTTPListener httpListener) {

        String base = baseURL.trim();
        base = base.endsWith("/") ? base.substring(0, base.length() - 1) : base;

        String url = String.format(LOCATION_PATH, base, clientUserId);

        new HTTPAsyncTask().execute(method, url, token, requestBody, httpListener);
    }

    private static class HTTPAsyncTask extends AsyncTask<Object, Void, JSONObject> {
        HTTPListener httpListener = null;

        @Override
        protected JSONObject doInBackground(Object... objects) {
            JSONObject response = null;

            try {
                RequestMethod method = (RequestMethod) objects[0];
                String urlSpec = (String) objects[1];
                String token = (String) objects[2];
                JSONObject requestBody = (JSONObject) objects[3];
                httpListener = (HTTPListener) objects[4];

                URL url = new URL(urlSpec);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                response = sendRequest(method, connection, requestBody, token);
            } catch (Exception e) {
                Log.w(TAG, e.getLocalizedMessage());
            }

            return response;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            if (response != null) {
                if (httpListener != null) {
                    try {
                        if (response.getBoolean("success")) {
                            httpListener.onSuccess(response.getInt("status_code"),
                                    response.getJSONObject("response_body"));
                        } else {
                            httpListener.onFail(response.getInt("status_code"),
                                    response.getString("status_message"));
                        }
                    } catch (JSONException e) {
                        Log.w(getClass().getSimpleName(), e.getMessage());
                        httpListener.onError(e.getMessage());
                    }
                }
            } else if (httpListener != null) {
                httpListener.onError("No response body");
            }
        }
    }

    /**
     * Reads POST response and based on the result creates a JSONObject to return.
     *
     * @param requestType String            The type of http method in the request.
     * @param connection  HttpURLConnection A URLConnection with support for HTTP-specific features.
     * @param jsonObject  JSONObject        JSON required for the API call
     * @return JSONObject The JSON response.
     * @throws IOException The exception that handles the failure of an input or output operation.
     */
    private static JSONObject sendRequest(RequestMethod requestType, HttpURLConnection connection,
                                         JSONObject jsonObject, String token) throws IOException {

        connection.setConnectTimeout(10000);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("X-Auth-Token", token);
        connection.setDoInput(true);
        InputStream is;
        String responseBody = "";

        try {
            Log.d(TAG, "===============================================");
            Log.d(TAG, "HTTP REQUEST: " + requestType + " " + connection.getURL().getPath());
            Log.d(TAG, "HTTP REQUEST: BODY: " + jsonObject);

            connection.setRequestMethod(requestType.toString());

            if (requestType != RequestMethod.GET) {
                connection.setDoOutput(true);
                OutputStream os = connection.getOutputStream();
                os.write(jsonObject.toString().getBytes("UTF-8"));
                os.close();
            }

            // Read response
            is = connection.getInputStream();
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            is = connection.getErrorStream();
        }

        if (is != null) {
            StringBuilder sb = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = "";

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            reader.close();
            is.close();
            responseBody = sb.toString();
        }

        connection.disconnect();
        return returnObject(connection, responseBody);
    }

    /**
     * Reads POST response and based on the result creates a JSONObject to return.
     *
     * @param connection   HttpURLConnection A URLConnection with support for HTTP-specific
     *                                       features.
     * @param responseBody String            The response that comes from central.
     * @return JSONObject The JSON response that comes from central.
     */
    private static JSONObject returnObject(HttpURLConnection connection, String responseBody) {
        JSONObject returnObject = new JSONObject();

        try {
            String message = connection.getResponseMessage();
            int httpResponseCode = connection.getResponseCode();

            try {
                Log.d(TAG, "HTTP RESPONSE: " + httpResponseCode + " - " + message);
                Log.d(TAG, "HTTP RESPONSE: Response Body: " + responseBody);

                if ((httpResponseCode >= 200 && httpResponseCode <= 299)
                        || (httpResponseCode >= 400 && httpResponseCode <= 499)) {

                    // if the response is 204 No Content response body is empty
                    // create a success response and return
                    if (httpResponseCode == 204) {
                        returnObject.put("success", "true");
                        returnObject.put("status_code", 0);
                        return returnObject;
                    }

                    JSONObject obj = new JSONObject(responseBody.trim());
                    int statusCode = obj.getInt("status_code");

                    // if the status field is "error", create a failure response and return
                    if (obj.getString("status").equalsIgnoreCase("error")) {
                        String responseMsg = obj.getString("status_message");
                        return getErrorResponseObject(returnObject, statusCode, responseMsg);
                    }

                    // if status code is 0, put all the fields in the responseBody json in an
                    // object called "response_body"
                    if (statusCode == 0) {
                        Iterator keys = obj.keys();
                        JSONObject body = new JSONObject();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();

                            if (key.equalsIgnoreCase("status")) {
                                returnObject.put("success", "true");
                            } else {
                                if (key.equalsIgnoreCase("status_code")) {
                                    returnObject.put("status_code", httpResponseCode);
                                } else {
                                    body.put(key, obj.get(key));
                                }
                            }
                        }

                        returnObject.put("response_body", body);
                    }
                } else {
                    // httpCode is not 2xx or 4xx
                    return getErrorResponseObject(returnObject, httpResponseCode, message);
                }

                return returnObject;

            } catch (JSONException ex) {
                Log.w(TAG, "HTTP Response Error: " + ex.getLocalizedMessage());
                return getErrorResponseObject(returnObject, httpResponseCode, message);
            }
        } catch (IOException ex) {
            String ioError = "HTTP Response IO Failure";
            Log.e(TAG, ioError + ex.getLocalizedMessage());

            // default error object because something is wrong with the httpconnection
            return getErrorResponseObject(returnObject, 500, ioError);
        }
    }

    private static JSONObject getErrorResponseObject(JSONObject returnObject, int statusCode, String message) {
        try {
            returnObject.put("success", "false");
            returnObject.put("status_code", statusCode);
            returnObject.put("status_message", message);
        } catch (JSONException ex) {
            Log.e(TAG, "Very bad JSON Exception. This should never happen!!!");
        }

        return returnObject;
    }
}

# FPGLocation

A framework for collecting and reporting user locations. GPS coordinates enhance the First Performance platform and allow for smarter fraud prevention and merchant data cleansing.

## Requirements

* Minimum SDK 16 (Jelly Bean)
* Kotlin >= 1.3.61
* Gradle Build Tools >= 3.6.2

## Installation

#### Import Module

* Import the `aar` file into your project as an android module. It can be found in the `Downloads` section in Bitbucket.
* Include the module in your `build.gradle` dependencies.

```js
// Replace 'module-name' with the name you gave the module when importing
dependencies {
  api project(':<module-name>')
}
```

#### Dependencies

FPGLocation relies on other Android components to run. Add them to your `build.gradle` file:

```js
implementation 'com.google.android.gms:play-services-location:17.0.0'
implementation 'androidx.localbroadcastmanager:localbroadcastmanager:1.0.0'
```

#### Declare Service

Declare the service and the intent filter in your top-level application Android Manifest inside the `<application>` tag.

```xml
<service
    android:name="com.fpg.geolocation.service.FPLocationService"
    android:foregroundServiceType="location"
    android:exported="true"
    android:enabled="true"/>

<receiver android:enabled="true" android:name="com.fpg.geolocation.service.FPBootReceiver">
    <intent-filter>
        <action android:name="android.intent.action.BOOT_COMPLETED"/>
    </intent-filter>
</receiver>
```

## Setup

FPGLocation can work with any configuration of location usage permissions, but works best when granted "Fine" and "Background" permissions.

#### Requiring Permissions

Add at least one of these location permissions to your `AndroidManifest.xml` file:
```xml
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```

This line is required for location events to continue to be recorded while the app is in the background:
```xml
<uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
```

Foreground services permissions:
```xml
<uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
<uses-permission android:name="android.permission.WAKE_LOCK" />
```

Boot start permission:
```xml
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
```

There are versions of android that performs battery saving mode. This permission can be used along with the `Intent` to request your application to be whitelisted in doze mode.
Kindly see this [documentation](https://developer.android.com/training/monitoring-device-state/doze-standby) about using this. **This is not recommended by Google, so use with caution.**
```xml
<uses-permission android:name="android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS"/>
```


#### Granting Permissions

Requesting and granting permissions are handled in your application level. FPGLocation will not function if your app has not been granted location permissions. A typical location permission request looks like this:

```java
ActivityCompat.requestPermissions(<activity>, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION }, INT_REQUEST_CODE);
```

Override `onRequestPermissionResult` to determine how the user handled the Android permission dialogue.  If the user granted permission to use location, FPLocationManager can safely begin monitoring location.
```java
@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
  if (requestCode == REQUEST_CODE) {
    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
      // Permission granted, do something.
    }
  }
}
```

## Usage

### Initialization

In order to start using FPGLocation, some initial setup is required. There are two ways to use the library. Either by using the `FPLocationManager` or directly accessing `FPLocationService`.


### Option 1: Using FPLocationManager

Initialize your `FPLocationManager` by passing the required parameters.

#### Configuration

Set a configuration for the service and pass it to the `FPLocationManager` instance.

```java
// Create a service configuration and set some desired properties.
FPServiceConfiguration config = FPServiceConfiguration.load();
config.setBaseURL(<base_url>); // base URL to send the location information
config.setToken(<server_token>);
config.setClientUserId(<client_user_id>);
config.setSmallestDisplacement(<displacement_in_meters>); // desired distance change for the location update to happen
config.setInterval(<desired_time>); // desired time acceptable for updating location
config.setNotificationTitle(<title>); // desired notification title when location service is running in the background else none (will default to service name)
config.setNotificationMessage(<message>); // desired notification message when the location service is running in the background else none
config.setNotificationIconId(<icon_id>); // desired notification icon of the service else none (will default to a map icon)
config.setSendUpdatesOnExplicitRequests(<boolean>); // flag to override the location configuration conditions when calling the `requestLocationUpdates()` method and thus will send updates.

// changes to FPServiceConfiguration can still be made after passing it to the FPLocationManager instance - just be sure to save.
config.save(<context>);

// Create an FPLocationManager instance
// context: Context
// activityClass: the class reference to your main activity. MyActivity.getClass()
// config: the FPServiceConfiguration
// delegate: a class that conforms to the FPLocationManagerDelegate interface.
FPLocationManager locationManager = FPLocationManager.getInstance(context, activitClass, config, delegate);
```

#### Delegate Interface

FPLocationManager will notify you about location updates and service binding via it's delegate property. If you wish to listen for updates, your class needs to conform to the FPLocationManagerDelegate interface to receive them.

```java
public class MyClass implements FPLocationServiceManagerDelegate {
  @Override
  public void onLocationManagerUpdate(FPLocation fpLocation) {
    // Process new locations
  }

  @Override
  public void onLocationManagerFailure(FPLocationError fpLocationError) {
    // Process errors from FPGLocation
  }

  @Override
  public void onBind() {
    // This signals that the location service is ready. Make sure to check for this callback before performing any service calls.
  }
}
```

#### Getting Locations

FPLocationManager will not begin monitoring for locations until told to do so. Updates will continue until a request to remove the location updates from FPLocationManager is invoked. Switching to and from background mode is handled automatically.

```java
// To start receiving locations
locationManager.requestLocationUpdates();

// To stop receiving locations
locationManager.removeLocationUpdates();

// To unbind the location service. This must be placed inside your main activity's override methoid `onStop()`.
locationManager.unbindService(<context>);
```


### Option 2: Using FPLocationService

Initialize a `ServiceConnection` object to listen and obtain a service through binding.
An `FPServiceConfiguration` is also needed to supply the necessary properties for the service. Please see [Configuration].

#### Service Connection

```java
// Monitors the state of the connection to the service.
private final ServiceConnection serviceConnection = new ServiceConnection() {

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "service connected");
        FPLocationService.LocalBinder binder = (FPLocationService.LocalBinder)service;
        locationService = binder.getService();
        bound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        locationService = null;
        bound = false;
    }
};
```

#### Binding

Perform a bind to the service inside your main activity.

```java
@Override
protected void onStart() {
    super.onStart();
    bindService(new Intent(this, FPLocationService.class), serviceConnection, Context.BIND_AUTO_CREATE);
}
```

#### Unbinding

The method of unbinding the service can be done in your main activity by overriding `onStop()`.

```java
@Override
protected void onStop() {
  if (bound) {
      // Unbind from the service. This signals to the service that this activity is no longer
      // in the foreground, and the service can respond by promoting itself to a foreground
      // service.
      unbindService(serviceConnection);
      bound = false;
  }
  super.onStop();
}
```

#### Getting Locations

The location updates can be controlled through the service by calling the following methods:

```java
// To start receiving locations
locationService.requestLocationUpdates();

// To stop receiving locations
locationService.removeLocationUpdates();
```

Updates are sent through broadcasts so a `BroadcastReceiver` is needed to listen for location updates, otherwise this is not necessary.

Declare your broadcast receiver.
```java
private class MyReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {
      FPLocation location = intent.getParcelableExtra(FPLocationService.BROADCAST_KEY_LOCATION);
      if (location != null) {
          Log.d(TAG, "Received location: " + location.getLatitude() + ", " + location.getLongitude());
      }
  }
}
```

Register and unregister it through overriding the following methods in your class like activity.
```java
@Override
protected void onResume() {
    super.onResume();
    LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
            new IntentFilter(FPLocationService.BROADCAST_KEY_LOCATION));
}

@Override
protected void onPause() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
    super.onPause();
}
```

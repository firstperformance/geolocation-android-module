package com.firstperformance.geo;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fpg.geolocation.models.FPLocationError;
import com.fpg.geolocation.models.FPServiceConfiguration;
import com.fpg.geolocation.protocol.FPLocationManagerDelegate;
import com.fpg.geolocation.service.FPLocationService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * An example activity implementing the location library / service
 * Created by Bryn Trussell on 03 January 2020.
 */
public class MainActivity extends FragmentActivity implements OnMapReadyCallback, FPLocationManagerDelegate {
    private final String TAG = this.getClass().getSimpleName();
    private static final int PERMISSIONS_REQUEST_CODE = 123;

    public static final String CHANNEL_ID = "locationServiceChannel";
    private final int notificationId = 701;
    private int counter = 1;

    /* FPGLocation Required Properties */

    // The URL for recording location updates.
    // This URL will have the geolocation API path appended to it.
    private final String baseURL = "https://fp-vanilla-central-qa.firstperformance.com/api/v1/";

    // The x-auth-token to add to the location update HTTP Request header.
    private final String token = "";

    // A unique identifier for your company provided by FP.
    private String clientUserId = "app@dev.com";

    // The distance in meters for the location update to happen.
    private float displacementMeters = 200;

    // The time before recording a location update considering there is a displacement
    private double intervalMinutes = 2;

    // An FPLocationManager to handle location update events.
    // This is used when using FPLocationManager instead of directly using the FPLocationService.
//    private FPLocationManager fpLocationManager;

    // The service configuration;
    private FPServiceConfiguration configuration;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    /* UI Views */

    private GoogleMap mMap;
    private Button trackingButton, locationsListButton, clientButton;
    private EditText clientField;

    /* Persistent State Tracking */

    private final String KEY_SF_GEOLOCATION = "com.fp.geolocation";
    private final String KEY_GEO_TRACKING = "com.fp.geo.tracking";
    private boolean isTracking = false;


    private FPLocationService locationService;

    // Tracks the bound state of the service.
    private boolean bound = false;

    // Monitors the state of the connection to the service.
    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "service connected");
            FPLocationService.LocalBinder binder = (FPLocationService.LocalBinder) service;
            locationService = binder.getService();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            locationService = null;
            bound = false;
        }
    };

    /* Activity Lifecycle */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configuration = FPServiceConfiguration.load(this);
        myReceiver = new MyReceiver();

        trackingButton = findViewById(R.id.tracking_button);
        locationsListButton = findViewById(R.id.locations_button);
        clientField = findViewById(R.id.client_id);
        clientButton = findViewById(R.id.client_button);

        clientUserId = configuration.getClientUserId();
        clientField.setText(clientUserId);
        clientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clientId = clientField.getText().toString().trim();
                if (!clientId.isEmpty()) {
                    clientUserId = clientId;
                    configuration.setClientUserId(clientUserId);
                    configuration.save(MainActivity.this);
                }
            }
        });

        trackingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleTrackingButton();
            }
        });

        locationsListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showLocationsList();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setupManager();

        if (!hasPermissions()) {
            requestPermissions();
        }
        createNotificationChannel();

        updateTrackingState();
        setTrackingState(isTracking);
        setTrackingButton(isTracking);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, FPLocationService.class), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void setupManager() {
        configuration.setClientUserId(clientUserId);
        configuration.setActivityClassName(getClass().getName());
        configuration.setBaseURL(baseURL);
        configuration.setToken(token);
        configuration.setInterval(minutesToMilliseconds(intervalMinutes));
        configuration.setSmallestDisplacement(displacementMeters);
        configuration.setNotificationTitle("FP Notification Title");
        configuration.setNotificationIconId(android.R.drawable.ic_dialog_map);

        // Alternate approach of using FPLocationManager
//        fpLocationManager = FPLocationManager.getInstance(this, getClass(), configuration, this);

        // save configuration if FPLocationManager approach is not used.
        configuration.save(this);
    }

    private long minutesToMilliseconds(double minutes) {
        return (long) (minutes * 60000);
    }

    private void startTracking() {
        if (isTracking) {
            locationService.requestLocationUpdates();
            // alternative
            // fpLocationManager.requestLocationUpdates();
        } else {
            locationService.removeLocationUpdates();
            // alternative
            // fpLocationManager.removeLocationUpdates();
        }
    }

    /**
     * Called to determine when the service binds using FPLocationManager.
     */
    @Override
    public void onBind() {
    }

    /* Tracking State */
    private void updateTrackingState() {
        // initialize the isTracking flag from shared preferences
        SharedPreferences geoPref = getSharedPreferences(KEY_SF_GEOLOCATION, MODE_PRIVATE);
        isTracking = geoPref.getBoolean(KEY_GEO_TRACKING, false);
    }

    private void setTrackingState(boolean newState) {
        isTracking = newState;
        SharedPreferences geoPref = getSharedPreferences(KEY_SF_GEOLOCATION, MODE_PRIVATE);
        geoPref.edit().putBoolean(KEY_GEO_TRACKING, newState).apply();
    }

    private void setTrackingButton(boolean trackingIsOn) {
        if (trackingIsOn) {
            trackingButton.setText(R.string.geo_tracking_on);
        } else {
            trackingButton.setText(R.string.geo_tracking_off);
        }
    }

    private void toggleTrackingButton() {
        setTrackingState(!isTracking);
        setTrackingButton(isTracking);
        startTracking();
    }

    /* Location Permissions */

    /**
     * Requests location permissions. If this is a repeat request, provide the user with additional
     * context as to why the app is requesting location access.
     */
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.location_context_message);
            builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startLocationPermissionRequest();
                }
            });
            builder.show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Asks for location access. Process the results of this request in onRequestPermissionsResult.
     */
    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                PERMISSIONS_REQUEST_CODE);
    }

    /**
     * Informs the user that location permissions were denied.
     */
    private void handlePermissionsDenied() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.location_denied_message);
        builder.setPositiveButton(R.string.button_settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Build intent that displays the App settings screen.
                Intent intent = new Intent();
                intent.setAction(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package",
                        BuildConfig.APPLICATION_ID, null);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        builder.show();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // User interaction was interrupted or the request was cancelled
                Log.i(TAG, "Permission request failed to get a response.");

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                Log.i(TAG, "Permission Granted");
            } else {
                // Permission denied.
                Log.i(TAG, "Permission Denied");

                // Notify the user that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                handlePermissionsDenied();
            }
        } else {
            Log.w(TAG, "Unexpected Permission Request Result: " + requestCode);
        }
    }

    /**
     * Returns TRUE if location permissions have already been granted.
     */
    private boolean hasPermissions() {
        final int GRANTED = PackageManager.PERMISSION_GRANTED;

        String fineKey = Manifest.permission.ACCESS_FINE_LOCATION;
        String coarseKey = Manifest.permission.ACCESS_COARSE_LOCATION;
        String backgroundKey = Manifest.permission.ACCESS_BACKGROUND_LOCATION;

        boolean hasFine = ActivityCompat.checkSelfPermission(this, fineKey) == GRANTED;
        boolean hasCoarse = ActivityCompat.checkSelfPermission(this, coarseKey) == GRANTED;
        boolean hasBackground = ActivityCompat.checkSelfPermission(this, backgroundKey) == GRANTED;

        return hasFine || hasCoarse || hasBackground;
    }

    /* Google Maps */

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "Map View is ready!");
        mMap = googleMap;

        setupMapView();
    }

    private void setupMapView() {
        Log.d(TAG, "Setting up map view");

        if (mMap == null || !hasPermissions()) {
            Log.w(TAG, "Setup called before map view was ready");
            return;
        }

        // show user location
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    /**
     * Updates the user interface to show the data from the given location.
     *
     * @param location the location data to show
     */
    private void updateUserInterfaceWithLocation(Location location) {
        // make sure the map view has loaded before adding the location
        if (mMap == null) {
            Log.w(TAG, "Location can't be shown on a null Map View");
            return;
        }

        float zoom = 16.0f;

        LatLng coordinates = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(coordinates));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, zoom));
    }

    /* Notifications */

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Example Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) manager.createNotificationChannel(serviceChannel);
        }
    }

    private void notifyLocation(Location location) {
        buildNotification("Location Update - " + counter++,
                location.getLongitude() + ", " + location.getLatitude());
    }

    private void notifyError(FPLocationError fpError) {
        buildNotification("Location Error",
                fpError.getDescription());
    }

    private void buildNotification(String title, String body) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_stat_name);
        builder.setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        builder.setContentTitle(title);
        builder.setContentText(body);
        builder.setAutoCancel(true);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(defaultSoundUri);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager == null) {
            Log.w(TAG, "Push: sendNotification: Notification Service Unavailable");
            return;
        }

        notificationManager.notify(notificationId, builder.build());
    }

    /* FP Location Manager Service Delegate */

    /**
     * Location updates are delivered here. This is where the UI should
     * be updated to inform the user about the newest location.
     *
     * @param location the most recently recorded location
     */
    @Override
    public void onLocationManagerUpdate(Location location) {
        Log.i(TAG, "Location Updated");

        // Do something with the location
        updateUserInterfaceWithLocation(location);

        // Send a notification whenever a location comes in
        notifyLocation(location);
    }

    /**
     * Handle errors reported by the FPLocationManager, including errors from the remote recording
     * server and location permission errors.
     *
     * @param fpLocationError the error to handle
     */
    @Override
    public void onLocationManagerFailure(FPLocationError fpLocationError) {
        Log.i(TAG, "Location Failure: " + fpLocationError.name());

        // Send a notification for unknown errors
        notifyError(fpLocationError);

        // One of the possible errors is that location permission hasn't been granted
        // yet or only for foreground use. In this case, request permissions again.
        if (fpLocationError == FPLocationError.PERMISSIONS) {
            requestPermissions();
        }
    }

    /**
     * Receiver for broadcasts sent by {@link FPLocationService}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(FPLocationService.BROADCAST_KEY_LOCATION);
            if (location != null) {
                Log.d(TAG, "Received location: " + location.getLatitude() + ", " + location.getLongitude());
                // Do something with the location broadcast.
                // An example would be using the same notification provided by the library and providing a custom title and message.
//                configuration.setNotificationTitle("Location Update - " + (counter++));
//                configuration.setNotificationMessage(location.getLongitude() + ", " + location.getLatitude());
//                configuration.save(context);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(FPLocationService.BROADCAST_KEY_LOCATION));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onStop() {
        // When using FPLocationManager, unbind the service.
//        fpLocationManager.unbindService(this);
        if (bound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(serviceConnection);
            bound = false;
        }
        super.onStop();
    }
}
